package com.android.payday;

import android.content.Intent;
import android.databinding.DataBindingUtil;
import android.graphics.Color;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;

import com.android.payday.databinding.ActivityScanBinding;
import com.google.zxing.Result;

import me.dm7.barcodescanner.zxing.ZXingScannerView;

public class ScanActivity extends AppCompatActivity implements ZXingScannerView.ResultHandler {

    ActivityScanBinding binding;
    ZXingScannerView viewScanner;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        binding = DataBindingUtil.setContentView(this, R.layout.activity_scan);
        viewScanner = binding.viewScanner;
        viewScanner.setDrawingCacheBackgroundColor(Color.BLACK);
        viewScanner.setResultHandler(this);
        viewScanner.startCamera();

    }

    @Override
    public void handleResult(Result result) {
        startActivity(new Intent(this, BillActivity.class));
    }

    @Override
    protected void onPause() {
        super.onPause();
        viewScanner.stopCamera();
    }
}
