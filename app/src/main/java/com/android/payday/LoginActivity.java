package com.android.payday;

import android.app.Notification;
import android.app.NotificationManager;
import android.app.PendingIntent;
import android.content.Intent;
import android.databinding.DataBindingUtil;
import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.view.View;

import com.android.payday.databinding.ActivityLoginBinding;

public class LoginActivity extends AppCompatActivity implements View.OnClickListener {

    private ActivityLoginBinding binding;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        binding = DataBindingUtil.setContentView(this, R.layout.activity_login);
        binding.btnLogin.setOnClickListener(this);
        binding.txtSignup.setOnClickListener(this);
    }

    @Override
    public void onClick(View view) {
        switch (view.getId()) {
            case R.id.btnLogin:
                startActivity(new Intent(this, MainActivity.class));
                break;

            case R.id.txtSignup:
                Notification();
                break;
        }
    }

    public void Notification() {
        // Set Notification Title
        String strtitle = "Split bill from Ricky";
        // Set Notification Text
        String strtext = "Bill from Lagani Cafe";

        // Open NotificationView Class on Notification Click
        Intent intent = new Intent(this, BillSplitedActivity.class);

        PendingIntent pIntent = PendingIntent.getActivity(this, 0, intent,
                PendingIntent.FLAG_UPDATE_CURRENT);

        //Create Notification using NotificationCompat.Builder
        Notification builder = null;
        if (android.os.Build.VERSION.SDK_INT >= android.os.Build.VERSION_CODES.JELLY_BEAN) {
            builder = new Notification.Builder(this)
                    .setPriority(Notification.PRIORITY_HIGH)
                    .setContentTitle(strtitle)
                    .setContentText(strtext)
                    .setSmallIcon(R.drawable.ic_apps_login)
                    .setContentIntent(pIntent)
                    .addAction(R.drawable.ic_close_white, "Refuse", pIntent)
                    .addAction(R.drawable.ic_check_white, "Accept", pIntent).build();
        }


        NotificationManager notificationManager =
                (NotificationManager) getSystemService(NOTIFICATION_SERVICE);

        notificationManager.notify(0, builder);

    }
}
