package com.android.payday.dialog;

import android.app.DialogFragment;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.Window;
import android.widget.Button;
import android.widget.RelativeLayout;
import android.widget.Toast;

import com.android.payday.R;
import com.android.payday.listener.FriendOwesDialogListener;

/**
 * Created by AlfianTry on 11/09/2016.
 */
public class OweDialog extends DialogFragment {

    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        View rootView = inflater.inflate(R.layout.dialog_list_friends, container,
                false);

        RelativeLayout layoutFriend = (RelativeLayout) rootView.findViewById(R.id.layoutFriend);
        layoutFriend.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Toast.makeText(getActivity(), "Request sent!", Toast.LENGTH_SHORT).show();
                dismiss();
            }
        });

        getDialog().getWindow().requestFeature(Window.FEATURE_NO_TITLE);

        return rootView;
    }
}
