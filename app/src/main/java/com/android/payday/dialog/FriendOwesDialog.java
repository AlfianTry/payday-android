package com.android.payday.dialog;

import android.app.DialogFragment;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.Window;
import android.widget.Button;

import com.android.payday.R;
import com.android.payday.listener.FriendOwesDialogListener;

/**
 * Created by AlfianTry on 11/09/2016.
 */
public class FriendOwesDialog extends DialogFragment {

    private FriendOwesDialogListener activity;

    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        View rootView = inflater.inflate(R.layout.dialog_friend_owes, container,
                false);

        activity = (FriendOwesDialogListener) getActivity();

        Button btnAccept = (Button) rootView.findViewById(R.id.btnAccept);
        btnAccept.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                activity.onOweAccepted();
                dismiss();
            }
        });

        getDialog().getWindow().requestFeature(Window.FEATURE_NO_TITLE);

        return rootView;
    }
}
