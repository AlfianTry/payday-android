package com.android.payday.dialog;

import android.app.DialogFragment;
import android.content.Intent;
import android.os.Bundle;
import android.os.CountDownTimer;
import android.support.annotation.Nullable;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.Window;
import android.widget.TextView;

import com.android.payday.MainActivity;
import com.android.payday.R;
import com.github.jorgecastilloprz.FABProgressCircle;
import com.github.jorgecastilloprz.listeners.FABProgressListener;

/**
 * Created by AlfianTry on 11/09/2016.
 */
public class PayDialog extends DialogFragment {

    FABProgressCircle fabProgressCircle;
    TextView txtStatus;

    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        View rootView = inflater.inflate(R.layout.dialog_pay, container,
                false);

        getDialog().getWindow().requestFeature(Window.FEATURE_NO_TITLE);

        fabProgressCircle = (FABProgressCircle) rootView.findViewById(R.id.fabProgressCircle);
        txtStatus = (TextView) rootView.findViewById(R.id.txtStatus);

        fabProgressCircle.attachListener(new FABProgressListener() {
            @Override
            public void onFABProgressAnimationEnd() {
                txtStatus.setTextColor(getResources().getColor(R.color.holo_green_light));
                txtStatus.setText("Success");
                new CountDownTimer(1000, 500) {
                    public void onTick(long millisUntilFinished) {

                    }

                    public void onFinish() {
                        getActivity().startActivity(new Intent(getActivity(), MainActivity.class));
                    }

                }.start();
            }
        });

        new CountDownTimer(5000, 1000) {
            public void onTick(long millisUntilFinished) {
                if(millisUntilFinished>4900){
                    fabProgressCircle.show();
                }
            }

            public void onFinish() {
                fabProgressCircle.beginFinalAnimation();
            }

        }.start();



        return rootView;
    }
}
