package com.android.payday;

import android.accounts.AccountManager;
import android.content.Intent;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;

import com.android.payday.app.Apps;

public class SplashActivity extends AppCompatActivity {

    private AccountManager accountManager;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        accountManager = Apps.getInstance().getAccountManager();
        if(accountManager.getAccountsByType(getPackageName()).length>0){
            startActivity(new Intent(this,MainActivity.class));
        }else{
            startActivity(new Intent(this, LoginActivity.class));
        }
        finish();
    }
}
