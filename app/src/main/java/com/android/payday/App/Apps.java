package com.android.payday.app;

import android.accounts.AccountManager;
import android.app.Application;
import android.content.Context;
import android.content.Intent;

import com.android.payday.LoginActivity;
import com.android.payday.helper.ConnectivityReceiver;
import com.android.payday.helper.PreferenceManager;

/**
 * Created by AlfianTry on 09/09/2016.
 */
public class Apps extends Application {

    public static final String TAG = Apps.class
            .getSimpleName();

    private static Apps mInstance;

    private PreferenceManager pref;
    private AccountManager accountManager;

    public static synchronized Apps getInstance() {
        return mInstance;
    }

    @Override
    public void onCreate() {
        super.onCreate();
        mInstance = this;
        //ACRA.init(this);
    }

    public void setConnectivityListener(ConnectivityReceiver.ConnectivityReceiverListener listener) {
        ConnectivityReceiver.connectivityReceiverListener = listener;
    }

    public PreferenceManager getPrefManager() {
        if (pref == null) {
            pref = new PreferenceManager(this);
        }

        return pref;
    }

    public AccountManager getAccountManager() {
        if (accountManager == null) {
            accountManager = AccountManager.get(this);
        }

        return accountManager;
    }




    public void logout() {
        pref.clear();
        Intent intent = new Intent(this, LoginActivity.class);
        intent.setFlags(Intent.FLAG_ACTIVITY_NEW_TASK | Intent.FLAG_ACTIVITY_CLEAR_TASK);
        startActivity(intent);
    }

    @Override
    protected void attachBaseContext(Context base) {
        super.attachBaseContext(base);
        /*MultiDex.install(this);*/
    }
}
