package com.android.payday;

import android.app.Activity;
import android.content.Intent;
import android.databinding.DataBindingUtil;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.support.v7.widget.Toolbar;
import android.view.LayoutInflater;
import android.view.MenuItem;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.TextView;

import com.android.payday.databinding.ActivitySplitBinding;
import com.android.payday.model.Person;
import com.android.payday.widget.ContactsCompletionView;
import com.android.payday.widget.SlideToUnlock;
import com.tokenautocomplete.FilteredArrayAdapter;
import com.tokenautocomplete.TokenCompleteTextView;

public class SplitActivity extends AppCompatActivity implements SlideToUnlock.OnUnlockListener, TokenCompleteTextView.TokenListener<Person> {

    ActivitySplitBinding binding;
    Toolbar toolbar;

    ContactsCompletionView[] completionView;
    Person[] people = new Person[]{
            new Person("Ahmad Muktado"),
            new Person("Ricky Adam")
    };
    ArrayAdapter<Person> adapter;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        binding = DataBindingUtil.setContentView(this, R.layout.activity_split);

        toolbar = (Toolbar) binding.getRoot().findViewById(R.id.toolbar);
        TextView txtTitle = (TextView) toolbar.findViewById(R.id.txtTitle);
        txtTitle.setText("Split Bill");

        setSupportActionBar(toolbar);
        getSupportActionBar().setHomeButtonEnabled(true);
        getSupportActionBar().setDisplayHomeAsUpEnabled(true);
        setTitle("");

        binding.slidetounlock.setOnUnlockListener(this);

        adapter = new FilteredArrayAdapter<Person>(this, R.layout.partial_layout_person, people) {
            @Override
            public View getView(int position, View convertView, ViewGroup parent) {
                if (convertView == null) {

                    LayoutInflater l = (LayoutInflater)getContext().getSystemService(Activity.LAYOUT_INFLATER_SERVICE);
                    convertView = l.inflate(R.layout.partial_layout_person, parent, false);
                }

                Person p = getItem(position);
                ((TextView)convertView.findViewById(R.id.name)).setText(p.getName());

                return convertView;
            }

            @Override
            protected boolean keepObject(Person person, String mask) {
                mask = mask.toLowerCase();
                return person.getName().toLowerCase().startsWith(mask);
            }
        };

        completionView = new ContactsCompletionView[]{
                (ContactsCompletionView)findViewById(R.id.txtBiller1),
                (ContactsCompletionView)findViewById(R.id.txtBiller2),
                (ContactsCompletionView)findViewById(R.id.txtBiller3),
                (ContactsCompletionView)findViewById(R.id.txtBiller4)};

        for(ContactsCompletionView contactsCompletionView : completionView){
            contactsCompletionView.setAdapter(adapter);
            contactsCompletionView.setTokenListener(this);
            contactsCompletionView.setTokenClickStyle(TokenCompleteTextView.TokenClickStyle.Select);
        }


    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        switch (item.getItemId()) {
            case android.R.id.home:
                super.onBackPressed();
                break;
            default:
                return super.onOptionsItemSelected(item);
        }
        return false;
    }


    @Override
    public void onUnlock() {
        startActivity(new Intent(this, SplitStatusActivity.class));
    }

    private void updateTokenConfirmation() {
        StringBuilder sb = new StringBuilder("Current tokens:\n");
        /*for (Object token: completionView.getObjects()) {
            sb.append(token.toString());
            sb.append("\n");
        }*/

        //((TextView)findViewById(R.id.tokens)).setText(sb);
    }


    @Override
    public void onTokenAdded(Person token) {
        //((TextView)findViewById(R.id.lastEvent)).setText("Added: " + token);
        updateTokenConfirmation();
    }

    @Override
    public void onTokenRemoved(Person token) {
        //((TextView)findViewById(R.id.lastEvent)).setText("Removed: " + token);
        updateTokenConfirmation();
    }
}
