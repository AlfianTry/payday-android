package com.android.payday;

import android.content.Intent;
import android.databinding.DataBindingUtil;
import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.view.MenuItem;
import android.view.View;
import android.widget.TextView;
import android.widget.Toast;

import com.android.payday.databinding.ActivityBillSplitedBinding;
import com.android.payday.dialog.OweDialog;
import com.android.payday.widget.SlideToUnlock;

public class BillSplitedActivity extends AppCompatActivity implements SlideToUnlock.OnUnlockListener {

    ActivityBillSplitedBinding binding;
    Toolbar toolbar;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        binding = DataBindingUtil.setContentView(this, R.layout.activity_bill_splited);

        toolbar = (Toolbar) binding.getRoot().findViewById(R.id.toolbar);
        TextView txtTitle = (TextView) toolbar.findViewById(R.id.txtTitle);
        txtTitle.setText("Bill");

        setSupportActionBar(toolbar);
        getSupportActionBar().setHomeButtonEnabled(true);
        getSupportActionBar().setDisplayHomeAsUpEnabled(true);
        setTitle("");

        binding.slidetounlock.setOnUnlockListener(this);

        binding.btnSplit.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                startActivity(new Intent(BillSplitedActivity.this, SplitActivity.class));
            }
        });

        binding.btnOwes.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                OweDialog oweDialog = new OweDialog();
                oweDialog.show(getFragmentManager(), "Owe Dialog");
            }
        });
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        switch (item.getItemId()) {
            case android.R.id.home:
                startActivity(new Intent(this, MainActivity.class));
                finish();
                break;
            default:
                return super.onOptionsItemSelected(item);
        }
        return false;
    }

    @Override
    public void onUnlock() {
        Toast.makeText(this, "Paid", Toast.LENGTH_SHORT).show();
    }
}
