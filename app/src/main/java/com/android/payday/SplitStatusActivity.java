package com.android.payday;

import android.content.DialogInterface;
import android.content.Intent;
import android.databinding.DataBindingUtil;
import android.support.v7.app.AlertDialog;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.support.v7.widget.Toolbar;
import android.view.MenuItem;
import android.view.View;
import android.widget.TextView;

import com.android.payday.databinding.ActivitySplitStatusBinding;
import com.android.payday.dialog.FriendOwesDialog;
import com.android.payday.dialog.PayDialog;
import com.android.payday.listener.FriendOwesDialogListener;
import com.android.payday.widget.SlideToUnlock;

public class SplitStatusActivity extends AppCompatActivity implements
        SlideToUnlock.OnUnlockListener, FriendOwesDialogListener {

    ActivitySplitStatusBinding binding;
    Toolbar toolbar;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        binding = DataBindingUtil.setContentView(this, R.layout.activity_split_status);

        toolbar = (Toolbar) binding.getRoot().findViewById(R.id.toolbar);
        TextView txtTitle = (TextView) toolbar.findViewById(R.id.txtTitle);
        txtTitle.setText("Split Bill Status");

        setSupportActionBar(toolbar);
        getSupportActionBar().setHomeButtonEnabled(true);
        getSupportActionBar().setDisplayHomeAsUpEnabled(true);
        setTitle("");

        binding.slidetounlock.setOnUnlockListener(this);

        binding.btnRefresh.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                FriendOwesDialog dialog = new FriendOwesDialog();
                dialog.show(getFragmentManager(), "Friend Owes");
            }
        });
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        switch (item.getItemId()) {
            case android.R.id.home:
                AlertDialog.Builder builder = new AlertDialog.Builder(SplitStatusActivity.this);
                builder.setMessage("Are you sure you want to cancel the bill?");
                builder.setPositiveButton("YES", new DialogInterface.OnClickListener() {
                    @Override
                    public void onClick(DialogInterface dialog, int which) {
                        startActivity(new Intent(SplitStatusActivity.this, MainActivity.class));
                    }
                });
                builder.setNegativeButton("NO", new DialogInterface.OnClickListener() {
                    @Override
                    public void onClick(DialogInterface dialog, int which) {
                        dialog.dismiss();
                    }
                });
                AlertDialog dialog = builder.create();
                dialog.show();
                break;
            default:
                return super.onOptionsItemSelected(item);
        }
        return false;
    }

    @Override
    public void onUnlock() {
        PayDialog payDialog = new PayDialog();
        payDialog.show(getFragmentManager(), "Pay Dialog");
    }

    @Override
    public void onOweAccepted() {
        binding.txtCountFriend.setText("1/1");
    }

    @Override
    public void onOweRefused() {

    }
}
