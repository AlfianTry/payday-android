package com.android.payday.listener;

/**
 * Created by AlfianTry on 11/09/2016.
 */
public interface FriendOwesDialogListener {

    void onOweAccepted();

    void onOweRefused();
}
