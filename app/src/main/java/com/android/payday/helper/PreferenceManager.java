package com.android.payday.helper;

import android.content.Context;
import android.content.SharedPreferences;
import android.util.Log;

import com.android.payday.model.User;

import java.util.ArrayList;
import java.util.List;
import java.util.Set;
import java.util.TreeSet;

/**
 * Created by AlfianTry on 09/09/2016.
 */
public class PreferenceManager {

    // Sharedpref file name
    private static final String PREF_NAME = "com.circustudio.sociova_preferences";
    // All Shared Preferences Keys
    private static final String KEY_USER_ID = "user_id";
    private static final String KEY_USER_NAME = "user_name";
    private static final String KEY_USER_EMAIL = "user_email";
    private static final String KEY_NOTIFICATIONS = "notifications";
    // Shared Preferences
    SharedPreferences sharedPreferences;
    // Editor for Shared preferences
    SharedPreferences.Editor editor;
    // Context
    Context _context;
    // Shared sharedPreferences mode
    int PRIVATE_MODE = 0;
    private String TAG = PreferenceManager.class.getSimpleName();

    // Constructor
    public PreferenceManager(Context context) {
        this._context = context;
        sharedPreferences = _context.getSharedPreferences(PREF_NAME, PRIVATE_MODE);
        editor = sharedPreferences.edit();
    }

    public void storeUser(User user) {
        editor.putString(KEY_USER_ID, user.getId());
        editor.putString(KEY_USER_NAME, user.getName());
        editor.putString(KEY_USER_EMAIL, user.getEmail());
        editor.commit();

        Log.e(TAG, "User is stored in shared preferences. " + user.getName() + ", " + user.getEmail());
    }

    public User getUser() {
        if (sharedPreferences.getString(KEY_USER_ID, null) != null) {
            String id, name, email;
            id = sharedPreferences.getString(KEY_USER_ID, null);
            name = sharedPreferences.getString(KEY_USER_NAME, null);
            email = sharedPreferences.getString(KEY_USER_EMAIL, null);

            User user = new User(id, name, email);
            return user;
        }
        return null;
    }

    public void storeString(String key, String value) {
        editor.putString(key, value);
        editor.commit();
    }

    public String getString(String key) {
        return sharedPreferences.getString(key, "");
    }

    public void storeInt(String key, int value) {
        editor.putInt(key, value);
        editor.commit();
    }

    public int getInt(String key) {
        return sharedPreferences.getInt(key, 0);
    }

    public void storeFloat(String key, float value) {
        editor.putFloat(key, value);
        editor.commit();
    }

    public float getFloat(String key) {
        return sharedPreferences.getFloat(key, 0);
    }

    public void storeBoolean(String key, boolean value) {
        editor.putBoolean(key, value);
        editor.commit();
    }

    public boolean getBoolean(String key) {
        return sharedPreferences.getBoolean(key, false);
    }



    public void storeArrayString(String key, Set<String> value) {
        editor.putStringSet(key, value);
        editor.commit();
    }

    public Set<String> getArrayString(String key){
        Set<String> nullSet = new TreeSet<>();
        return sharedPreferences.getStringSet(key,nullSet);
    }

    public boolean saveArray(String key, List<String> sKey) {
        SharedPreferences.Editor mEdit1 = sharedPreferences.edit();
        mEdit1.putInt(key + "_size", sKey.size()); /* sKey is an array */

        for (int i = 0; i < sKey.size(); i++) {
            mEdit1.remove(key + "_" + i);
            mEdit1.putString(key + "_" + i, sKey.get(i));
        }

        return mEdit1.commit();
    }

    public List<String> loadArray(String key) {
        List<String> sKey = new ArrayList<>();
        sKey.clear();
        int size = sharedPreferences.getInt(key + "_size", 0);

        for (int i = 0; i < size; i++) {
            sKey.add(sharedPreferences.getString(key + "_" + i, null));
        }

        return sKey;
    }

    public boolean isContains(String key) {
        return sharedPreferences.contains(key);
    }



    public void addNotification(String notification) {

        // get old notifications
        String oldNotifications = getNotifications();

        if (oldNotifications != null) {
            oldNotifications += "|" + notification;
        } else {
            oldNotifications = notification;
        }

        editor.putString(KEY_NOTIFICATIONS, oldNotifications);
        editor.commit();
    }

    public String getNotifications() {
        return sharedPreferences.getString(KEY_NOTIFICATIONS, null);
    }

    public void removeItem(String key){
        editor.remove(key);
        editor.commit();
    }

    public void clear() {
        editor.clear();
        editor.commit();
    }

}
